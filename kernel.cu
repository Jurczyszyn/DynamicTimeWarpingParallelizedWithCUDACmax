#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <algorithm>
#include <iostream>
#include <string>
using namespace std;
using DataType = unsigned int;
using IndexType = unsigned int;
cudaError_t cmaxWithCuda(DataType *b, const DataType *a, unsigned int size);
__global__ void cmaxKernel(DataType *b, const DataType *a, const IndexType iteracja, const IndexType n)
{
	IndexType i = (blockIdx.x * blockDim.x) + threadIdx.x;
	IndexType x = iteracja - i;
	IndexType y = i;
	IndexType miejsceWWektorze = x + y*n;
	IndexType miejsceWWektorzePoLewej = x + y*n - (IndexType)1;
	IndexType miejsceWWektorzeOdDolu = x + (y- (IndexType)1)*n;
	IndexType miejsceWWektorzePoSkosie = x + (y- (IndexType)1)*n - (IndexType)1;
	if (x > 0 && y > 0
		&& x < n && y < n)
	{
		DataType minKoszt = min(b[miejsceWWektorzePoLewej], b[miejsceWWektorzeOdDolu]);
		b[miejsceWWektorze] = a[miejsceWWektorze] + min(minKoszt, b[miejsceWWektorzePoSkosie]);
	}
}
int printCmaxFail() {
	fprintf(stderr, "kernel cmaxWithCuda failed");
	system("pause");
	return 1;
}
cudaError_t printFail(cudaError_t status, string s) {
	cout << s << endl;
	system("pause");
	return status;
}
int main(int argc, char** argv)
{
	const int arraySize = 16;
	const DataType a[arraySize] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
	DataType b[arraySize] = { 0 };
	cudaError_t status = cmaxWithCuda(b, a, arraySize);
	if (status != cudaSuccess) return printCmaxFail();
	printf("cmax({ \n1\t2\t3\t4 \n5\t6\t7\t8 \n9\t10\t11\t12 \n13\t14\t15\t16 }) = {\n%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d}\n",
		b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7], b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15]);
	struct cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, 0);
	cout << "using " << properties.multiProcessorCount << " multiprocessors" << endl;
	cout << "max threads per processor: " << properties.maxThreadsPerMultiProcessor << endl;
	cout << "maxThreadsDim: " << properties.maxThreadsDim << endl;
	cout << "maxGridSize: " << properties.maxGridSize << endl;
	cout << "maxSurface1D: " << properties.maxSurface1D << endl;
	cout << "maxThreadsPerBlock: " << properties.maxThreadsPerBlock << endl;
	system("pause");
	const int rozmiarDuzy = 2000*2000;//1020 * 1020;//1000 * 1000;//123000;//12100;//2048
	const int nDuze = sqrt(rozmiarDuzy);
	DataType a_duze[rozmiarDuzy] = { 0 };
	DataType b_duze[rozmiarDuzy] = { 0 };
	for (int i = 0; i < rozmiarDuzy; i++)
	{
		a_duze[i] = i;
	}
	if(atoi(argv[1]) == 1)
	{
		cout << endl;
		cout << "cmax({\n";
		for (int i = 0; i < nDuze; i++)
		{
			for (int j = 0; j < nDuze; j++) {
				cout << a_duze[i*nDuze + j]<< "\t";
			}
			cout << endl;
		}
		cout << " }) = {\n";
	}
	status = cmaxWithCuda(b_duze, a_duze, rozmiarDuzy);
	if (status != cudaSuccess) return printCmaxFail();
	if (atoi(argv[1]) == 1)
	{
		for (int i = 0; i < nDuze; i++)
		{
			for (int j = 0; j < nDuze; j++) {
				cout << b_duze[i*nDuze + j] << "\t";
			}
			cout << endl;
		}
		cout << " }\n";
	}
	status = cudaDeviceReset();
	if (status != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset error");
		system("pause");
		return 1;
	}
	cout << "Prawdopodobnie sie udalo" << endl;
	cout << "Przedostatni element: " << b_duze[rozmiarDuzy - 2] << endl;
	cout << "Ostatni element: " << b_duze[rozmiarDuzy - 1] << endl;
	cout << "Srodkowy element: " << b_duze[rozmiarDuzy/2] << endl;
	cout << "Srodkowy element + 1: " << b_duze[rozmiarDuzy / 2 - 1] << endl;
	system("pause");
	return 0;
}
cudaError_t cmaxWithCuda(DataType *b, const DataType *a, unsigned int size)
{
	DataType *aOnCuda = 0;
	DataType *bOnCuda = 0;
	const int n = sqrt(size);
	cudaError_t status;
	struct cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, 0);
	unsigned int maxThreadsPerBlock = properties.maxThreadsPerBlock;
	unsigned int numBlocks = 1;
	if (maxThreadsPerBlock > 0 && size > maxThreadsPerBlock) {
		numBlocks = size / maxThreadsPerBlock;
	}
	else {
		fprintf(stderr, "Attention: Table size less than maxThreadsPerBlock not allowed!\n");
	}
	cout << "maxThreadsPerBlock: " << properties.maxThreadsPerBlock << endl;
	cout << "size: " << size << endl;
	cout << "numBlocks: " << numBlocks << endl;
	system("PAUSE");
	b[0] = a[0];
	for (int i = 1; i < n; i++)
	{
		b[i*n] = b[(i-1)*n] + a[i*n];
		b[i] = b[i-1] + a[i];
	}
	status = cudaSetDevice(0);
	if (status != cudaSuccess) return printFail(status, "cudaSetDevice error");
	status = cudaMalloc((void**)&aOnCuda, size * sizeof(DataType));
	if (status != cudaSuccess) return printFail(status, "cudaMalloc error");
	status = cudaMalloc((void**)&bOnCuda, size * sizeof(DataType));
	if (status != cudaSuccess) return printFail(status, "cudaMalloc error");
	status = cudaMemcpy(aOnCuda, a, size * sizeof(DataType), cudaMemcpyHostToDevice);
	if (status != cudaSuccess) return printFail(status, "cudaMemcpy error");
	status = cudaMemcpy(bOnCuda, b, size * sizeof(DataType), cudaMemcpyHostToDevice);
	if (status != cudaSuccess) return printFail(status, "cudaMemcpy error");
	for(int i=1;i<=2*n-1;i++)
	{
		cmaxKernel << <numBlocks, maxThreadsPerBlock >> >(bOnCuda, aOnCuda, i, n);
	}
	status = cudaGetLastError();
	if (status != cudaSuccess) return printFail(status, "cmaxKernel error");
	status = cudaDeviceSynchronize();
	if (status != cudaSuccess) return printFail(status, "cudaDeviceSynchronize error");
	status = cudaMemcpy(b, bOnCuda, size * sizeof(DataType), cudaMemcpyDeviceToHost);
	if (status != cudaSuccess) return printFail(status, "cudaMemcpy error");
	return status;
}